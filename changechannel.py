#coding=utf-8

import Tkinter
import tkMessageBox # Информационные окна
from showprobe import ShowProbe # Для отображения списка анализаторов
from parsingmgroup import ParsingProb # проверяем наличие обрабатываемого канала на анализаторах и возвращаем из модуля список пробов с флагами действий

'''Модуль будет либо добавлять канал, либо удалять'''

'''При запуске будет открывать окно со списком всех пробов, с возможностью сделать выбор всех
разом или ни одного, или самостоятельно проставить галки. Удаление/добавление будет проведено
только для выбранных анализаторов'''

class ChangeChannel():

	def __init__(self, ch, addr):
		print 'ch = ', ch , '(1 = add, 0 = del)'
		if addr == '':
			self.authorizationObject = 'local'
		else:
			self.authorizationObject = addr
			print 'load changechannel.py', ' + ', addr.a 
		if ch == 1: self.add()
		if ch == 0: self.delete()


	# Добавление
	def add(self):	
		addCh = Tkinter.Toplevel()
		addCh.title('Add Channel')
		# Название канала
		Tkinter.Label(addCh, text = 'Name channel to add:').pack(side = Tkinter.TOP)
		namech = Tkinter.Entry(addCh)
		namech.pack(side = Tkinter.TOP)
		# Мультикаст группа
		Tkinter.Label(addCh, text = 'IP group:').pack(side = Tkinter.TOP)
		ipch = Tkinter.Entry(addCh)
		ipch.pack(side = Tkinter.TOP)
		# Порт
		Tkinter.Label(addCh, text = 'Port:').pack(side = Tkinter.TOP)
		portch = Tkinter.Entry(addCh)
		portch.pack(side = Tkinter.TOP)
		# Join or Not
		# Будет сделано через Checkbutton
		var = Tkinter.IntVar() # по умолчанию не выбран
		check = Tkinter.Checkbutton(addCh, text = 'Join?..' , variable = var)
		check.pack(side = Tkinter.TOP)

		show = ShowProbe(addCh,1)
		show.pack(side = Tkinter.TOP)

		# Index - Нужно не забыть получить текущий максимальный и плюсовать к нему 1-цу
		button_add_ch = Tkinter.Button(addCh, text = 'Add', command = (lambda : self.changech(namech.get(),ipch.get(),portch.get(),var.get(),'add' , show.getIndexList() )))
		button_add_ch.pack(expand = Tkinter.YES,fill = Tkinter.X)  


		addCh.focus_set()
		addCh.grab_set() 
		addCh.wait_window()



#Ниже будет выводится прокручивающийся список со всеми анализаторами,
#так же будет кнопка выбора всех - All и ни одного - None, можно будет поставить 
#флаги на всех анализаторах, к которым нужно будет применить изменения

	# Удаление
	def delete(self):
		delCh = Tkinter.Toplevel()
		delCh.title('Delete Channel')
		Tkinter.Label(delCh, text = 'IP group to remove:').pack(side = Tkinter.TOP)
		ipgroup = Tkinter.Entry(delCh)
		ipgroup.pack(side = Tkinter.TOP)

		show = ShowProbe(delCh,1)
		show.pack(side = Tkinter.TOP)

		button_del_ch = Tkinter.Button(delCh, text = 'Delete', command = (lambda: self.changech('',ipgroup.get(),'','','del' , show.getIndexList())))
		button_del_ch.pack(expand = Tkinter.YES,fill = Tkinter.X)

		delCh.focus_set()
		delCh.grab_set() 
		delCh.wait_window()
		
#Ниже будет выводится прокручивающийся список со всеми анализаторами,
#так же будет кнопка выбора всех - All и ни одного - None, можно будет поставить 
#флаги на всех анализаторах, к которым нужно будет применить изменения  '''  

	def changech(self, name, group, port, join, flag , listIndex):
		if flag == 'del':
			# запускаем соответствующий код по удалению канала с выбранных анализаторов
			print ('group delete')
			print 'Выбранные анализаторы (индексы)'
			for i in listIndex:
				print i
			d = ParsingProb(listIndex, flag, name, group, port, join, self.authorizationObject) # из parsingmgroup


		if flag == 'add':
			# запускаем соответствующий код по добавлению канала на выбранные анализаторы
			print ('group add', 'join = ', join)
			print 'Выбранные анализаторы (индексы)'
			for i in listIndex:
				print i
			a = ParsingProb(listIndex, flag, name, group, port, join, self.authorizationObject) # из parsingmgroup