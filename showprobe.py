#coding=utf-8

'''Подкласс фрейма, будет прикреплятся к главному окну в добавлении и удалении пробов
для отображения списка анализаторов'''

import Tkinter
import pickle
import tkMessageBox # Информационные окна
import listbox


class ShowProbe(Tkinter.Frame):
	def __init__(self, parent, ch):
		Tkinter.Frame.__init__(self, parent)
		#self.pack() непонятно зачем нужно
		if ch != 0: # не показываем при добавлении новых пробов - не нужно
			self.var = Tkinter.StringVar()
			self.var.set(None) # ничего не выбрано по умолчанию
			Tkinter.Radiobutton(self,text="Select all  ", variable = self.var, value = 'all', command = self.all).pack(side = Tkinter.TOP,expand = Tkinter.YES,fill = Tkinter.X)
			Tkinter.Radiobutton(self,text="Select none ", variable = self.var, value = 'none', command = self.none).pack(side = Tkinter.TOP,expand = Tkinter.YES,fill = Tkinter.X)
		try:
			dbfile = open('dbprobe', 'r')
			DB = pickle.load(dbfile)
			dbfile.close()
			self.s = listbox.ScrolledList(self)
			for key in DB:
				self.s.append(key)
		except:
			tkMessageBox.showinfo(title = 'Load List Probe', message = 'ERROR. May be file is empty.')

# Обновления списка при добавлении/удалении анализаторов - ДОДЕЛАТЬ!!!
	def popList(self):
		pass

	def getIndexList(self):
		temp = self.s.handleList()
		return temp

	def all(self):
		self.s.all()

	def none(self):
		self.s.none()

	def close(self):
		self.destroy()


# Для тестирования

def test():
    root = Tkinter.Tk()
    myTest = ShowProbe(root,'none').pack()
    return root

def main():
    root = test()
    root.mainloop()

if __name__ == '__main__':
    main()	