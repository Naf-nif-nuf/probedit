# probedit
This utility to add / remove multicast groups Bridgetech analyzers. - status: in the pipeline

Start program - runprobedit.py 
Tested in Python 2.7.6 - Win7 and OS X 10.10.4

Нужно сделать:

+ нет номера порта при добавлении в запросе! Как? - нашёл /probe/core/EthernetDoc/selChannelDoc&xml=1&index=234&markAsNew=1&Edit=1&port=1234&join=true
+ получение индекса текущей группы для удаления
+ получение максимального индекса для добавления
+ локальный логин без VBC и удаление групп
+ скачиваем eth_streamlist_ex.xml в рабочую директорию или считываем для обработки
+ формируем данные для нового телеканала
- загружаем файл eth_streamlist_ex.xml на анализатор
- добавить возможность ввода номера страницы для канала и задать значение по умолчанию
- добавить возможность задания SD, HD (пока по-умолчанию делаю default)
- логин через VBC и удаление групп
- логин
- регулярные выражения для проверки вводимых данных
- отдельные потоки для добавления/удаления?
