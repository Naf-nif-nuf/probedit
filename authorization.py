#coding=utf-8

import Tkinter
import tkMessageBox # Информационные окна

'''Модуль, отвечающий за авторизацию на VBC'''

class LoginToVBC():
	def __init__(self):
		self.l = '' # login
		self.p = ''	# password
		self.a = '' # address
		self.statusLogin = 0 # по дефолту
		loginVBC = Tkinter.Toplevel()
		loginVBC.title('Login to VBC')
		Tkinter.Label(loginVBC, text = 'Address:').pack(side = Tkinter.TOP)
		self.vbcAddress = vbcAddress = Tkinter.Entry(loginVBC)
		vbcAddress.pack(side = Tkinter.TOP)
		Tkinter.Label(loginVBC, text = 'Login:').pack(side = Tkinter.TOP)
		self.vbcLogin = vbcLogin = Tkinter.Entry(loginVBC)
		vbcLogin.pack(side = Tkinter.TOP)
		Tkinter.Label(loginVBC, text = 'Password:').pack(side = Tkinter.TOP)
		self.vbcPassword = vbcPassword = Tkinter.Entry(loginVBC)
		vbcPassword.pack(side = Tkinter.TOP)
		loginVBCbutton = Tkinter.Button(loginVBC, text = 'Login', command = (lambda: self.login(vbcAddress.get(), vbcLogin.get(),vbcPassword.get() )))
		loginVBCbutton.pack()
		print 'load authorization.py'
		loginVBC.focus_set()
		loginVBC.grab_set() 
		loginVBC.wait_window()

	# Залогиниваемся на VBC
	def login(self, address, login, password):
		# тут авторизуемся и если успешно, то устанавливаем flag = 1
		self.a = address # для использования в runprobedit и передачи в parsingmgroup
		self.statusLogin = statusLogin = 1
		if statusLogin == 1:
			print 'login successfully'
			tkMessageBox.showinfo(title = 'About', message = 'Login successfully!')
		elif statusLogin == 0:
			print 'login fails'
			tkMessageBox.showinfo(title = 'About', message = 'Login fails!')
