#coding=utf-8
import pickle
import tkMessageBox # Информационные окна
from urllib import urlopen, urlencode
import urllib2
import cookielib

'''Модуль будет загружать xml конфиг с анализатора, модифицировать его и заливать обратно
на анализатор.
Ссылка на eth_streamlist_ex.xml с информацией о join-ах:
http://10.127.235.2/probe?xml=1&exportMask=0x80&saveas=eth_streamlist_ex.xml&xmlFlags=0x10&&


POST запрос
http://10.127.235.2/probe/core/importExport/data.xml?probe&

http://10.127.235.2/probe/core/importExport/data.xml

'''

print 'load parsingmgroup'

'''
	data['id'] = ''
	data['name'] = ''
	data['port'] = ''
	data['join'] = ''
'''

class ParsingProb():

	def __init__(self, listProbe, flag, name, group, port, join, vbcAdd):
		print 'Address VBC  :   ' + vbcAdd	# проверка
		if vbcAdd == 'local':
			self.vbcURL = vbcAdd
		else:
			self.vbcURL = vbcAdd.a # сохраняем адрес
		try:
			dbfile = open('dbprobe', 'r')
			self.DB = DB = pickle.load(dbfile)
			dbfile.close()
		except:
			tkMessageBox.showinfo(title = 'ERROR', message = 'File not found')
		# Проверка
		for i in listProbe:
			print i, ' : ' ,DB[i], 'only IP : ' , DB[i]['ip']

		if flag == 'del':
			self.delete(listProbe, group, '')

		if flag == 'add':
			self.add(listProbe, name, group, port, join)

	# Тут формируется окончательная ссылка для ДОБАВЛЕНИЯ канала
	def add(self, listProbe, name, group, port, join):

		for i in listProbe: # выбираю первый анализатор в списке

			cookie = cookielib.CookieJar() # требуется создать объект, который будет хранить наши куки, этот объект создается из библиотеки cookielib
			req = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookie)) # После этого, создаем объект opener из urllib2, собственно, для общения по протоколу http с добавлением cookie хэндлера
			# Чтобы не было проблем с некоторыми веб-серверами, добавим юзер агент, пораспространеннее		
			req.addheaders = [('User-Agent', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.11) Gecko/20101012 Firefox/3.6.11'), ] 
			# и установим наш opener как глобальный (необязательно)
			#urllib2.install_opener(req)

			# ЧЕРЕЗ VBC
			# посмотреть как выглядит запрос с нестандартными портами так как он такой fwd?url=http://10.14.76.250/probe/core/
			if self.vbcURL != 'local':
				if self.DB[i]['port'] == '80':
					loadXMLURL = 'http://'+ self.vbcURL + '/vbc/fwd?url=' + 'http://' + self.DB[i]['ip'] + '/probe?xml=1&exportMask=0x80&saveas=eth_streamlist_ex.xml&xmlFlags=0x10&&'
				else:
					loadXMLURL = 'http://'+ self.vbcURL + '/vbc/fwd?url=' + 'http://' + self.DB[i]['ip']+ ':' +DB[i]['port'] + '/probe?xml=1&exportMask=0x80&saveas=eth_streamlist_ex.xml&xmlFlags=0x10&&'
				if join == '1': join = 'true'
				if join == '0': join = 'false' 				

			# НАПРЯМУЮ ЧЕРЕЗ АНАЛИЗАТОРЫ
			if self.vbcURL == 'local':
				if self.DB[i]['port'] == '80':
					loadXMLURL = 'http://' + self.DB[i]['ip'] + '/probe?xml=1&exportMask=0x80&saveas=eth_streamlist_ex.xml&xmlFlags=0x10&&'
				else:
					loadXMLURL = 'http://' + self.DB[i]['ip']+ ':' +DB[i]['port'] + '/probe?xml=1&exportMask=0x80&saveas=eth_streamlist_ex.xml&xmlFlags=0x10&&'
				if join == '1': join = 'true'
				if join == '0': join = 'false' 

				# авторизуемся на анализаторе
				resp = req.open('http://' + self.DB[i]['ip'] + '/probe/core/setup/login?Login=true&&passwd=QAZxswEDC&submitflash=login') 
				print 'COOKIE : ',cookie
				print 'CODE AUTH: ' , resp.getcode()

				# проверяем есть ли уже канал на анализаторе
				index = self.searchIndex(group, self.DB[i]['ip']) # отдаю его и его ip для получения индекса канала на нём
				# проверяем
				print 'TEST. GROUP : ', group, 'INDEX : ', index
				if index != '':
					# тогда удаляем канал
					finalURL = 'http://' + self.DB[i]['ip'] + '/probe/core/EthernetDoc/' + 'mcastnames?Delete=1&&selectedIndex=' + str(index)
					deleteGroup = req.open(finalURL)

			# Теперь считываем файл по loadXMLURL в список строк
			page = urlopen(loadXMLURL)
			dataList = page.readlines()
			
			# открываем для чтения локальный файл и сохраняем скачиваемые строки в него
			output = open('eth_streamlist_ex.xml', 'w')
			x = 0
			y = 0
			lenght = 0
			for i in dataList:
				# во второй строке удаляем crc=""
				if 'crc="' in i:
					x = len(i)
					y = len(self.split_text(i, 'crc="' ))
					for symbol in self.split_text(i, 'crc="' ):
						lenght += 1
						if symbol == '"':
							break
					lenght += 5 # не подсчитанные crc="
					i = i[0: x - y - 5] + i[x - y - 4 +  lenght:]
					output.write(i)
					continue # пропускаю виток

				# дойдя до <mclist xmlChildren="list"> после неё записываем данные добавляемого канала
				if '<mclist xmlChildren="list">' in i:
					output.write(i)
					i = '\t'*3 + '<mcastChannel name="' + name + '" addr="' + group + '" port="' + port + '" ssmAddr="0.0.0.0" ssmName="source1" ssmAddr2="0.0.0.0"' + 'ssmName2="source2" ssmAddr3="0.0.0.0" ssmName3="source3" ssmAddr4="0.0.0.0" ssmName4="source4" ssmAddr5="0.0.0.0" ssmName5="source5"' + 'tpreset="Default" vbcpreset="Default" etrEngine="1" join="' + join + '" enableFec="false" page="20" dispPage="20" t2miStream="" t2miPid="4096" t2miPlp="-1"></mcastChannel>' + '\n'
					output.write(i)
					continue # пропускаю виток
				# остальные строки дописываем как есть
				output.write(i)

			# Загружаем конфиг на анализатор
			params = { 'fileupload': open('eth_streamlist_ex.xml', 'r') }
			data_encoded = urlencode(params)
			#datagen, headers = urlencode(params)
			#request = urllib2.Request('http://10.127.235.2/probe/core/importExport/data.xml?probe&', datagen, headers)
			#upload = req.open('http://10.127.235.2/probe/core/importExport/data.xml?probe&', {'file':open('eth_streamlist_ex.xml', 'rb')} )
			#result = req.open(request)
			upl = req.open('http://10.127.235.2/probe/core/importExport/data.xml?probe&', data_encoded)
			print 'CODE UPLOAD : ',upl.getcode()


	# Тут формируется окончательная ссылка для УДАЛЕНИЯ канала
	def delete(self, listProbe, group, index):
		for i in listProbe:
			if index == '':
				index = self.searchIndex(group, self.DB[i]['ip'])

			# ЧЕРЕЗ VBC
			# посмотреть как выглядит запрос с нестандартными портами так как он такой fwd?url=http://10.14.76.250/probe/core/
			if self.vbcURL != 'local':
				if self.DB[i]['port'] == '80':
					finalURL = 'http://'+ self.vbcURL + '/vbc/fwd?url=' + 'http://' + self.DB[i]['ip'] + '/probe/core/EthernetDoc/'
				else:
					finalURL = 'http://'+ self.vbcURL + '/vbc/fwd?url=' + 'http://' + self.DB[i]['ip']+ ':' +DB[i]['port'] + '/probe/core/EthernetDoc/'

			# НАПРЯМУЮ ЧЕРЕЗ АНАЛИЗАТОРЫ
			if self.vbcURL == 'local':
				if self.DB[i]['port'] == '80':
					finalURL = 'http://' + self.DB[i]['ip'] + '/probe/core/EthernetDoc/'
				else:
					finalURL = 'http://' + self.DB[i]['ip']+ ':' +DB[i]['port'] + '/probe/core/EthernetDoc/'

				# авторизуемся на анализаторе
				resp = req.open('http://' + self.DB[i]['ip'] + '/probe/core/setup/login?Login=true&&passwd=QAZxswEDC&submitflash=login') 
				print 'COOKIE : ',cookie
				print 'CODE AUTH: ' , resp.getcode()


			finalURL = finalURL + 'mcastnames?Delete=1&&selectedIndex=' + str(index)
			print finalURL
			# теперь ТУТ удаляем по итоговой ссылке
			resp = req.open(finalURL) 


	# Вернёт индекс для канала в xml доке на пробе
	def searchIndex(self, group, ip):
		if self.vbcURL == 'local':
			request = 'http://' + str(ip) + '/probe/data/channels'
		else:
			request = 'http://'+ self.vbcURL + '/vbc/fwd?url=' +  'http://' + str(ip) + '/probe/data/channels'
		page = urlopen(request)
		dataList = page.readlines()
		out = ''
		for line in dataList:
			if group in line:
				if ' id="' in line:
					for symbol in self.split_text(line, ' id="'):
						if symbol == '"':
							break
						out+=symbol
		page.close()
		index = out
		return index
		
	def split_text(self, full_line, small_line):
		lenght = len(small_line)
		x = full_line.find(small_line)
		full_line = full_line[lenght + x :]
		return full_line

# тестирование
if __name__ == '__main__':
	#temp = ParsingProb(['Bryansk','Ivanovo','Astrakhan'], 'add' , 'ST_Perviy', '233.33.210.86' , '5050', '0', 'local')
	temp = ParsingProb(['Bryansk'], 'add' , 'test123', '233.33.33.33' , '5050', '1', 'local')

	#temp.add('','','','','')
	#temp.delete('','')

	#temp1 = ParsingProb(['Bryansk'], 'del' , '', '233.33.33.33' , '', '', 'local')
	#temp1.add('','','','','')
	#temp1.delete('','')


