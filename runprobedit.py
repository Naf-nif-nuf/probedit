#coding=utf-8

import Tkinter
import tkMessageBox # Информационные окна
import parsingmgroup # парсинга групп с анализаторов для получения id группы

import changechannel # для добавления/удаления
import authorization # для авторизации
import addnewprobe # добавление/удаление анализаторов в локальный файл dbprobe

'''Программа для удаления/добавления телеканалов на анализаторы Bridgetech'''
'''Тестировалась в Python 2.7.6 Mac OS X 10.10.3'''

'''Модальные окна в OS X - Окно не в фокусе будет доступно, но нажатия на кнопки
не обрабатываются, в Windows всё ок.'''

'''Нужна проверка вводимых значений с помощью регулярных выражений!!!'''

'''
ip, port, name
Bryansk = { 'ip':'10.127.235.2' , 'port':'80' , 'pass':'' }
Astrakhan = { 'ip':'10.127.210.134' , 'port':'80' , 'pass':'' }
Ivanovo = { 'ip':'10.196.72.6' , 'port':'80' , 'pass':'' }
ПРИМЕР:
('Bryansk', {'ip': '10.127.235.2', 'port': '80', 'pass': 'elvis'})
'''

class Gui(Tkinter.Frame):
    def __init__(self, parent):
        Tkinter.Frame.__init__(self, parent)
        #self.pack() непонятно зачем нужно
        # Кнопки 
        self.test = ''
        self.login_to_VBC = login_to_VBC = Tkinter.Button(self, text = 'Login to VBC', command = self.accessToVBC )
        login_to_VBC.pack(expand = Tkinter.YES,fill = Tkinter.X)

        # для логина локального без VBC, будут использоваться пароли сохранённые в файле с анализаторами
        local = Tkinter.IntVar()
        self.localCheck = localCheck = Tkinter.Checkbutton(self, text = 'Local login(Not VBC)' , variable = local,command = (lambda : self.localEnable( local.get() ) ) ) 
        localCheck.pack(side = Tkinter.TOP)

        add_new_Probe = Tkinter.Button(self, text = 'Add Probe', command = (lambda: addnewprobe.AddNewProbe(1)))
        add_new_Probe.pack(expand = Tkinter.YES,fill = Tkinter.X)

        add_new_Probe = Tkinter.Button(self, text = 'Delete Probe', command = (lambda: addnewprobe.AddNewProbe(0)))
        add_new_Probe.pack(expand = Tkinter.YES,fill = Tkinter.X)        

        self.add_ch = add_ch = Tkinter.Button(self, text = 'Add Channel', command = (lambda : changechannel.ChangeChannel(1, self.test)))
        # блокируем кнопки до авторизации
        add_ch.config(state = Tkinter.DISABLED)
        add_ch.pack(expand = Tkinter.YES,fill = Tkinter.X)        

        self.delete_ch = delete_ch = Tkinter.Button(self, text = 'Delete Channel', command = (lambda : changechannel.ChangeChannel(0, self.test)))
        # блокируем кнопки до авторизации
        delete_ch.config(state = Tkinter.DISABLED)
        delete_ch.pack(expand = Tkinter.YES,fill = Tkinter.X)

        about = Tkinter.Button(self, text = 'About', command = self.show_about)
        about.pack(expand = Tkinter.YES,fill = Tkinter.X)

    def show_about(self):
        tkMessageBox.showinfo(title = 'About', message = 'shcherbakov.dm.v@gmail.com')

    def accessToVBC(self):
        self.test = authorization.LoginToVBC()
        if self.test.statusLogin == 1:
            # разблокируем кнопки
            self.add_ch.config(state = Tkinter.NORMAL)
            self.delete_ch.config(state = Tkinter.NORMAL)
            self.localCheck.config(state = Tkinter.DISABLED)

        # проверка что вернулось
        print 'Address VBC server : ' , self.test.a


    # разблокируем кнопки для локального логина на пробы
    def localEnable(self, l):
        if l == 1:
            self.add_ch.config(state = Tkinter.NORMAL)
            self.delete_ch.config(state = Tkinter.NORMAL)
            self.login_to_VBC.config(state = Tkinter.DISABLED)
        if l == 0:
            self.add_ch.config(state = Tkinter.DISABLED)
            self.delete_ch.config(state = Tkinter.DISABLED)  
            self.login_to_VBC.config(state = Tkinter.NORMAL)


if __name__ == '__main__':
    main = Tkinter.Tk()
    # Главное окно
    main.title('Probeedit')
    main.geometry('200x350') # Задаём размер окна
    prob = Gui(main)
    prob.config(relief = Tkinter.SUNKEN)
    prob.pack(expand = Tkinter.YES,fill = Tkinter.BOTH)
    prob.mainloop()