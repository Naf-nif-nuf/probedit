#coding=utf-8

import pickle
import Tkinter
import tkMessageBox # Информационные окна
from showprobe import ShowProbe # Для отображения списка анализаторов

class AddNewProbe():
    
    def __init__(self, ch):
        if ch == 1:self.add()
        if ch == 0:self.delete()
        print 'load addnewprobe.py'

    def add(self):
        self.addprobe = addprobe =  Tkinter.Toplevel()
        addprobe.title('Add new Probe')
        # Имя добавляемого проба
        Tkinter.Label(addprobe, text = 'Name Probe:').pack(side = Tkinter.TOP)
        nameProbe = Tkinter.Entry(addprobe)
        nameProbe.pack(side = Tkinter.TOP)
        # IP
        Tkinter.Label(addprobe, text = 'IP address:').pack(side = Tkinter.TOP)
        ipProbe = Tkinter.Entry(addprobe)
        ipProbe.pack(side = Tkinter.TOP)
        # Порт
        Tkinter.Label(addprobe, text = 'Port:').pack(side = Tkinter.TOP)
        portProbe = Tkinter.Entry(addprobe)
        portProbe.pack(side = Tkinter.TOP)
        # Пароль 
        Tkinter.Label(addprobe, text = 'Password:').pack(side = Tkinter.TOP)
        passProbe = Tkinter.Entry(addprobe)
        passProbe.pack(side = Tkinter.TOP)

        # Кнопка добавления
        button_add_probe = Tkinter.Button(addprobe, text = 'Add', command = (lambda : self.addnewProbeToDB(nameProbe.get(), ipProbe.get(), portProbe.get(), passProbe.get(), clear)))
        button_add_probe.pack(side = Tkinter.TOP,expand = Tkinter.YES,fill = Tkinter.X)
        # для очистки введённых значений
        clear = [nameProbe, ipProbe, portProbe, passProbe]

        self.show = ShowProbe(addprobe, 0)
        self.show.pack(side = Tkinter.TOP) 

#Ниже будет выводится прокручивающийся список со всеми анализаторами,
#так же будет кнопка выбора всех - All и ни одного - None, можно будет поставить 
#флаги на всех анализаторах, к которым нужно будет применить изменения  '''

        addprobe.focus_set() # Передает окну фокус ввода приложения
        addprobe.grab_set()  # Блокирует доступ ко всем другим окнам приложения, пока не будет закрыто данное окно
        addprobe.wait_window() # Приостанавливает вызвавшую программу, пока не будет уничтожен виджет


    def delete(self):
        self.delprobe = delprobe = Tkinter.Toplevel()
        delprobe.title('Delete Probe')
        # Имя добавляемого проба
        Tkinter.Label(delprobe, text = 'Name Probe:').pack(side = Tkinter.TOP)
        nameProbe = Tkinter.Entry(delprobe)
        nameProbe.pack(side = Tkinter.TOP)
        # Кнопка удаления по имени или ip
        button_del_probe = Tkinter.Button(delprobe, text = 'Delete', command = (lambda: self.delProbeFromDB(nameProbe.get(), clear )))
        button_del_probe.pack(side = Tkinter.TOP,expand = Tkinter.YES,fill = Tkinter.X) 

        clear = [nameProbe]
        
        self.show = show = ShowProbe(delprobe, 1)
        show.pack(side = Tkinter.TOP) 
        delprobe.focus_set()
        delprobe.grab_set()  
        delprobe.wait_window()

#Ниже будет выводится прокручивающийся список со всеми анализаторами,
#так же будет кнопка выбора всех - All и ни одного - None, можно будет поставить 
#флаги на всех анализаторах, к которым нужно будет применить изменения  '''       

    # добавляем анализатор
    def addnewProbeToDB(self,name, ip, port, passw, clear):
        try:
            dbfile = open('dbprobe', 'r')
            DB = pickle.load(dbfile)
        except:
            dbfile = open('dbprobe', 'w')
        # Временный вариант проверки на вхождение
        if name in DB or ip in DB:
            # Выводим окно что введённое Name Probe уже есть в базе
            tkMessageBox.showinfo(title = 'ERROR', message = 'This name or ip already exists')
            dbfile.close()     	
        elif name == '' or ip == '':
            tkMessageBox.showinfo(title = 'ERROR', message = 'Please check Name and IP')
        else: 
            # Иначе добавляем в базу
            dbfile = open('dbprobe', 'w')
            if passw == '': passw = 'elvis'
            if port == '': port = '80'
            DB[name] = { 'ip':ip  , 'port':port , 'pass':passw }
            pickle.dump(DB, dbfile)
            print 'm-group has been added'
            dbfile.close()
            tkMessageBox.showinfo(title = 'Successfully!', message = 'Probe has been add')
            for x in clear: x.delete(0, 'end')
            self.show.close()
            self.show = ShowProbe(self.addprobe, 0)
            self.show.pack(side = Tkinter.TOP) 



    # удаляем анализатор
    def delProbeFromDB(self, name, clear):
        try:
            dbfile = open('dbprobe', 'r')
            DB = pickle.load(dbfile)
        except:
            tkMessageBox.showinfo(title = 'ERROR', message = 'File not found')

        '''# Временный вариант проверки на вхождение
        if not DB[name]:
            # Выводим окно что введённое Name Probe отсутствует в базе
            tkMessageBox.showinfo(title = 'ERROR', message = 'Data not found')'''

        # подумать как сделать нормальную проверку
        if name == '':
            # делаем запрос есть ли выделенные элементы в Листбоксе, если есть - удаляем
            # если нет - выводим сообщение об ошибке
            if self.show.s.handleList() == []:
                tkMessageBox.showinfo(title = 'ERROR', message = 'Please check Name')
            else:
                deleteList = self.show.s.handleList()
                dbfile = open('dbprobe', 'w')
                for i in deleteList:
                    DB.pop(i)
                pickle.dump(DB, dbfile)
                print 'Probe has been delete'
                dbfile.close()
                tkMessageBox.showinfo(title = 'Successfully!', message = 'Probe has been delete')
                self.show.close()
                self.show = ShowProbe(self.delprobe, 1)
                self.show.pack(side = Tkinter.TOP)                     
        else: 
            # Иначе удаляем из базы
            dbfile = open('dbprobe', 'w')
            DB.pop(name)
            pickle.dump(DB, dbfile)
            print 'Probe has been delete'
            dbfile.close()
            tkMessageBox.showinfo(title = 'Successfully!', message = 'Probe has been delete')
            clear[0].delete(0, 'end')
            self.show.close()
            self.show = ShowProbe(self.delprobe, 1)
            self.show.pack(side = Tkinter.TOP) 

