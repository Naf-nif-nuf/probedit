#coding=utf-8

# http://src.gnu-darwin.org/ports/math/py-mpz/work/Python-2.3.4/Lib/idlelib/ScrolledList.py.html

from Tkinter import *

class ScrolledList:

    default = "(None)"

    def __init__(self, master, **options):
        # Create top frame, with scrollbar and listbox
        self.master = master
        self.frame = frame = Frame(master)
        self.frame.pack(fill="both", expand=1)
        self.vbar = vbar = Scrollbar(frame, name="vbar")
        self.vbar.pack(side="right", fill="y")
        self.listbox = listbox = Listbox(frame, exportselection=0,
            background="white")
        if options:
            listbox.configure(options)
        listbox.pack(expand=1, fill="both")
        # Tie listbox and scrollbar together
        vbar["command"] = listbox.yview
        listbox["yscrollcommand"] = vbar.set
        listbox.config(selectmode = MULTIPLE, setgrid = 1) # MULTIPLE - выбор по одному элементу щелчком
        # Mark as empty
        self.clear()

    # Получаем выделенное
    def handleList(self):
        elementList = []
        indexList = self.listbox.curselection() # curselection -  для получения индексов, get - для элементов списка
        for i in indexList:
            elementList.append( self.listbox.get(i) )
        return elementList

    # Формируем Листбокс
    def append(self, item):
        if self.empty:
            self.listbox.delete(0, "end")
            self.empty = 0
        self.listbox.insert("end", str(item))

    # Выбираем все элементы в списке
    def all(self):
        self.listbox.selection_set(0, last="end")

    # Снимаем выбор со всех 
    def none(self):
        self.listbox.selection_clear(0, last="end")

    def close(self):
        self.frame.destroy()

    def clear(self):
        self.listbox.delete(0, "end")
        self.empty = 1
        self.listbox.insert("end", self.default)

    def get(self, index):
        return self.listbox.get(index)

    def select(self, index):
        self.listbox.focus_set()
        self.listbox.activate(index)
        self.listbox.selection_clear(0, "end")
        self.listbox.selection_set(index)
        self.listbox.see(index)

